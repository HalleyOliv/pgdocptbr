### Introdução

Páginas Web da documentação do PostgreSQL 14.5 traduzida para o
Português do Brasil.

### Sítios na Web

- GitLab: [Documentação do PostgreSQL 14.5](https://halleyoliv.gitlab.io/pgdocptbr)
- Netlify: [Documentação do PostgreSQL 14.5](https://pgdocptbr.xyz)
