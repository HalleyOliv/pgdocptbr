------------------------------------------------------------------------
--
-- advanced.sql
--    Tutorial sobre recursos avançados do PostgreSQL
--
--
-- Copyright (c) 1994, Regents of the University of California
-- Tradução: Halley Pacheco de Oliveira, 2022.
--
-- src/tutorial/advanced.source
-- $ psql --file=advanced.sql --dbname=meu_bd
--
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Herança:
--  Uma tabela pode herdar de zero ou mais tabelas. Uma consulta pode
--  fazer referência a todas as linhas de uma tabela ou a todas as
--  linhas de uma tabela mais todas as linhas de suas descendentes.
------------------------------------------------------------------------

-- Por exemplo, a tabela capitais herda da tabela cidades. (Ela herda
-- todos os campos de dados de cidades.)

CREATE TABLE cidades (
    nome        text,
    populacao   float8,
    altitude    int     -- (em pés)
);

CREATE TABLE capitais (
    estado       char(2) UNIQUE NOT NULL
) INHERITS (cidades);

-- Agora são adicionados dados às tabela

INSERT INTO cidades VALUES ('São Francisco', 7.24E+5, 63);
INSERT INTO cidades VALUES ('Las Vegas', 2.583E+5, 2174);
INSERT INTO cidades VALUES ('Mariposa', 1200, 1953);

INSERT INTO capitais VALUES ('Sacramento', 3.694E+5, 30, 'CA');
INSERT INTO capitais VALUES ('Madison', 1.913E+5, 845, 'WI');

SELECT * FROM cidades;
SELECT * FROM capitais;

-- Podem ser encontradas todas as cidades, incluindo as capitais,
-- localizadas a uma altitude igual ou superior a 500 pés, usando:

SELECT c.nome, c.altitude
FROM cidades c
WHERE c.altitude > 500;

-- Para varrer apenas as linhas da tabela mãe, é usado ONLY:

SELECT nome, altitude
FROM ONLY cidades
WHERE altitude > 500;

-- Limpeza (devem ser removidas as tabelas filhas primeiro)

DROP TABLE capitais;
DROP TABLE cidades;
