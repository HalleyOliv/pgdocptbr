------------------------------------------------------------------------
--
-- basics.sql
--    Tutorial básico do PostgreSQL
--   (criação de tabelas e manipulação de dados)
--
-- Tradução: Halley Pacheco de Oliveira, 2022.
--
-- src/tutorial/basics.source
-- $ psql --file=basico.sql --dbname=meu_bd
--
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Criação de tabela:
-- O comando CREATE TABLE é usado para criar tabelas. O PostgreSQL
-- tem seu próprio conjunto de tipos de dados nativo.
-- (Observe que o SQL não diferencia letras maiúsculas de minúsculas.)
------------------------------------------------------------------------

CREATE TABLE clima (
    cidade          varchar(80),    -- nome da cidade
    temp_min        int,            -- temperatura mínima em °C
    temp_max        int,            -- temperatura máxima em °C
    prcp            real,           -- precipitação em mm
    data            date            -- data
);

CREATE TABLE cidades (
    nome            varchar(80),    -- nome da cidade
    localizacao     point           -- coordenadas x, y (Longitude, Latitude)
);


------------------------------------------------------------------------
-- Preenchimento de uma tabela com linhas:
--  É usada a instrução INSERT para inserir uma nova linha em uma
--  tabela. Existem várias maneiras de especificar para quais colunas
--  os dados devem ir.
------------------------------------------------------------------------

-- 1. O caso mais simples é quando a lista de valores corresponde à
-- ordem das colunas especificadas em CREATE TABLE.

INSERT INTO clima
    VALUES ('Salvador - BA', 21, 28, 3.4, '2022-06-25');

INSERT INTO cidades
    VALUES ('Salvador - BA', '(-38.48806, -13.01477)');

-- 2. Também pode ser especificado a qual coluna os valores correspondem.
--    (As colunas podem ser especificadas em qualquer ordem.
--    Também pode ser omitido qualquer número de colunas, por exemplo,
--    precipitação desconhecida abaixo.

INSERT INTO clima (data, cidade, temp_min, temp_max, prcp)
    VALUES ('2022-06-26', 'Salvador - BA', 23, 27, 0);

INSERT INTO clima (data, cidade, temp_min, temp_max)
    VALUES ('2022-06-29', 'Rio de Janeiro - RJ', 17, 29);


------------------------------------------------------------------------
-- Consulta a uma tabela:
--  A instrução SELECT é usada para recuperar os dados.
--  A sintaxe básica é SELECT colunas FROM tabelas WHERE predicados.
------------------------------------------------------------------------

-- Uma instrução simples seria:

SELECT * FROM clima;

-- Também é possível especificar expressões na lista de destino.
-- ('AS coluna' especifica o nome da coluna no resultado. É opcional.)

SELECT cidade,(temp_min+temp_max)/2 AS temp_media, data FROM clima;

-- Se for desejado recuperar linhas que satisfaçam a determinada condição
-- (ou seja, uma restrição), a condição deve ser especificada na cláusula WHERE.
-- O seguinte comando recupera o clima de Salvador - BA em dias chuvosos:

SELECT * FROM clima
    WHERE cidade = 'Salvador - BA' AND prcp > 0;

-- A seguir está um exemplo mais complicado. As linhas duplicadas são
-- removidas quando DISTINCT é especificado. ORDER BY especifica a coluna
-- para ordenar. (Apenas para garantir que esse comando não cause confusão,
-- DISTINCT e ORDER BY podem ser usados separadamente.)

SELECT DISTINCT cidade
    FROM clima
    ORDER BY cidade;


------------------------------------------------------------------------
-- Junções entre tabelas:
--  as consultas podem acessar várias tabelas ao mesmo tempo, ou acessar
--  a mesma tabela de forma que várias instâncias da tabela sejam
--  processadas ao mesmo tempo.
------------------------------------------------------------------------

-- Essa consulta junta a tabela clima com a tabela cidades.

SELECT * FROM clima JOIN cidades ON cidade = nome;

-- Essa forma evita a duplicação das colunas contendo o nome da cidade:

SELECT cidade, temp_min, temp_max, prcp, data, localizacao
    FROM clima JOIN cidades ON cidade = nome;

-- como os nomes das colunas são todos diferentes, não é necessário
-- especificar o nome da tabela. Se quiser ser claro, pode ser
-- feito como mostrado abaixo. Os resultados são idênticos, é claro.

SELECT clima.cidade, clima.temp_min, clima.temp_max,
       clima.prcp, clima.data, cidades.localizacao
    FROM clima JOIN cidades ON cidade = nome;

-- Sintaxe antiga de junção

SELECT *
    FROM clima, cidades
    WHERE cidade = nome;

-- Junção externa

SELECT *
    FROM clima LEFT OUTER JOIN cidades ON (clima.cidade = cidades.nome);

-- Supondo que se deseje encontrar todos os registros que estão na faixa
-- de temperatura de outros registros. C1 e C2 são aliases para clima.

SELECT C1.cidade, C1.temp_min AS menor, C1.temp_max AS maior,
    C2.cidade, C2.temp_min AS menor, C2.temp_max AS maior
    FROM clima C1, clima C2
    WHERE C1.temp_min < C2.temp_min
    AND C1.temp_max > C2.temp_max;

------------------------------------------------------------------------
-- Funções de agregação
------------------------------------------------------------------------

SELECT max(temp_min)
    FROM clima;

SELECT cidade FROM clima
    WHERE temp_min = (SELECT max(temp_min) FROM clima);

-- Agregação com GROUP BY
SELECT cidade, max(temp_min)
    FROM clima
    GROUP BY cidade;

-- ... e HAVING
SELECT cidade, max(temp_min)
    FROM clima
    GROUP BY cidade
    HAVING max(temp_min) < 20;

------------------------------------------------------------------------
-- Atualizações:
--  É usada  a instrução UPDATE para atualizar dados.
------------------------------------------------------------------------

-- Supondo ter sido descoberto que as leituras de temperatura realizadas
-- após 25 de junho de 2022 estão todas 2 graus mais altas, os dados
-- podem ser atualizados da seguinte maneira:

UPDATE clima
    SET temp_max = temp_max - 2,  temp_min = temp_min - 2
    WHERE data > '2022-06-25';

SELECT * FROM clima;


------------------------------------------------------------------------
-- Exclusões:
--  É usada a instrução DELETE para excluir linhas de uma tabela.
------------------------------------------------------------------------

-- Supondo que não há mais interesse no clima do Rio de Janeiro - RJ,
-- então pode ser executado o comando a seguir para excluir essas linhas
-- da tabela:

DELETE FROM clima WHERE cidade = 'Rio de Janeiro - RJ';

SELECT * FROM clima;

-- Também podem ser excluídas todas as linhas em uma tabela fazendo o
-- seguinte. (Isso é diferente de DROP TABLE, que remove a tabela além
-- de remover as linhas.)

DELETE FROM clima;

SELECT * FROM clima;


------------------------------------------------------------------------
-- Remoção das tabelas:
--  A instrução DROP TABLE é usada para remover tabelas. Depois de ter
--  feito isso, não será mais possível usar essas tabelas.
------------------------------------------------------------------------

DROP TABLE clima, cidades;
