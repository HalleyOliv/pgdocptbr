CREATE TABLE salario_emp (
    nome_dep      varchar(80),
    num_emp       int,
    salario       int,
    data_contrat  date
);

INSERT INTO salario_emp VALUES ('desenvolvimento'   ,    11 ,   5200, '2000-01-01');
INSERT INTO salario_emp VALUES ('desenvolvimento'   ,     7 ,   4200, '2001-01-01');
INSERT INTO salario_emp VALUES ('desenvolvimento'   ,     9 ,   4500, '2002-01-01');
INSERT INTO salario_emp VALUES ('desenvolvimento'   ,     8 ,   6000, '2003-01-01');
INSERT INTO salario_emp VALUES ('desenvolvimento'   ,    10 ,   5200, '2004-01-01');
INSERT INTO salario_emp VALUES ('pessoal'           ,     5 ,   3500, '2001-01-01');
INSERT INTO salario_emp VALUES ('pessoal'           ,     2 ,   3900, '2002-01-01');
INSERT INTO salario_emp VALUES ('vendas'            ,     3 ,   4800, '2000-01-01');
INSERT INTO salario_emp VALUES ('vendas'            ,     1 ,   5000, '2001-01-01');
INSERT INTO salario_emp VALUES ('vendas'            ,     4 ,   4800, '2002-01-01');

SELECT nome_dep, num_emp, salario, avg(salario) OVER (PARTITION BY nome_dep) FROM salario_emp;

SELECT nome_dep, num_emp, salario,
       rank() OVER (PARTITION BY nome_dep ORDER BY salario DESC)
FROM salario_emp;

SELECT salario, sum(salario) OVER () FROM salario_emp;

SELECT salario, sum(salario) OVER (ORDER BY salario) FROM salario_emp;

SELECT nome_dep, num_emp, salario, data_contrat
FROM
  (SELECT nome_dep, num_emp, salario, data_contrat,
          rank() OVER (PARTITION BY nome_dep ORDER BY salario DESC, num_emp) AS pos
     FROM salario_emp
  ) AS ss
WHERE pos < 3;

SELECT sum(salario) OVER w, avg(salario) OVER w
  FROM salario_emp
  WINDOW w AS (PARTITION BY nome_dep ORDER BY salario DESC);

DROP TABLE salario_emp;
