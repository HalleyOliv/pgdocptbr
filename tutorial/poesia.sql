INSERT INTO literatura VALUES (
'Olavo Bilac',
'Via-Láctea',
'poesia',
$$Ora (direis) ouvir estrelas! Certo
Perdeste o senso!” E eu vos direi, no entanto,
Que, para ouvi-las, muita vez desperto
E abro as janelas, pálido de espanto…

E conversamos toda a noite, enquanto
A via-láctea, como um pálio aberto,
Cintila. E, ao vir do sol, saudoso e em pranto,
Inda as procuro pelo céu deserto.

Direis agora: “Tresloucado amigo!
Que conversas com elas? Que sentido
Tem o que dizem, quando estão contigo?”

E eu vos direi: “Amai para entendê-las!
Pois só quem ama pode ter ouvido
Capaz de ouvir e de entender estrelas$$
);

INSERT INTO literatura VALUES (
'Luís Vaz de Camões',
'Mudam-se os tempos',
'poesia',
$$Mudam-se os tempos, mudam-se as vontades,
Muda-se o ser, muda-se a confiança:
Todo o mundo é composto de mudança,
Tomando sempre novas qualidades.

Continuamente vemos novidades,
Diferentes em tudo da esperança:
Do mal ficam as mágoas na lembrança,
E do bem (se algum houve) as saudades.

O tempo cobre o chão de verde manto,
Que já coberto foi de neve fria,
E em mim converte em choro o doce canto.

E afora este mudar-se cada dia,
Outra mudança faz de mor espanto,
Que não se muda já como soía.$$
);

INSERT INTO literatura VALUES (
'Luís Vaz de Camões',
'Amor é um fogo que arde sem se ver',
'poesia',
$$Amor é um fogo que arde sem se ver;
É ferida que dói, e não se sente;
É um contentamento descontente;
É dor que desatina sem doer.

É um não querer mais que bem querer;
É um andar solitário entre a gente;
É nunca contentar-se e contente;
É um cuidar que ganha em se perder;

É querer estar preso por vontade;
É servir a quem vence, o vencedor;
É ter com quem nos mata, lealdade.

Mas como causar pode seu favor
Nos corações humanos amizade,
Se tão contrário a si é o mesmo Amor?$$
);

INSERT INTO literatura VALUES (
'Fernando Pessoa',
'Mar Português',
'poesia',
$$Ó mar salgado, quanto do teu sal
São lágrimas de Portugal!
Por te cruzarmos, quantas mães choraram,
Quantos filhos em vão rezaram!
Quantas noivas ficaram por casar
Para que fosses nosso, ó mar!

Valeu a pena? Tudo vale a pena
Se a alma não é pequena.
Quem quer passar além do Bojador
Tem que passar além da dor.
Deus ao mar o perigo e o abismo deu,
Mas nele é que espelhou o céu.$$
);
