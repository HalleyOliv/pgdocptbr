CREATE TABLE estados (
    estado  text PRIMARY KEY,
    capital text,
    sigla   text
);

INSERT INTO estados VALUES ('Acre','Rio Branco','AC');
INSERT INTO estados VALUES ('Alagoas','Maceió','AL');
INSERT INTO estados VALUES ('Amapá','Macapá','AP');
INSERT INTO estados VALUES ('Amazonas','Manaus','AM');
INSERT INTO estados VALUES ('Bahia','Salvador','BA');
INSERT INTO estados VALUES ('Ceará','Fortaleza','CE');
INSERT INTO estados VALUES ('Distrito Federal','Brasília','DF');
INSERT INTO estados VALUES ('Espírito Santo','Vitória','ES');
INSERT INTO estados VALUES ('Goiás','Goiânia','GO');
INSERT INTO estados VALUES ('Maranhão','São Luís','MA');
INSERT INTO estados VALUES ('Mato Grosso','Cuiabá','MT');
INSERT INTO estados VALUES ('Mato Grosso do Sul','Campo Grande','MS');
INSERT INTO estados VALUES ('Minas Gerais','Belo Horizonte','MG');
INSERT INTO estados VALUES ('Pará','Belém','PA');
INSERT INTO estados VALUES ('Paraíba','João Pessoa','PB');
INSERT INTO estados VALUES ('Paraná','Curitiba','PR');
INSERT INTO estados VALUES ('Pernambuco','Recife','PE');
INSERT INTO estados VALUES ('Piauí','Teresina','PI');
INSERT INTO estados VALUES ('Rio de Janeiro','Rio de Janeiro','RJ');
INSERT INTO estados VALUES ('Rio Grande do Norte','Natal','RN');
INSERT INTO estados VALUES ('Rio Grande do Sul','Porto Alegre','RS');
INSERT INTO estados VALUES ('Rondônia','Porto Velho','RO');
INSERT INTO estados VALUES ('Roraima','Boa Vista','RR');
INSERT INTO estados VALUES ('Santa Catarina','Florianópolis','SC');
INSERT INTO estados VALUES ('São Paulo','São Paulo','SP');
INSERT INTO estados VALUES ('Sergipe','Aracaju','SE');
INSERT INTO estados VALUES ('Tocantins','Palmas','TO');
